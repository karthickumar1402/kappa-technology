
exports.getCurrentDateTime = function()
    {
        var currentdate = new Date();
        var datetime = String(currentdate.getDate()).padStart(2, '0') + "/"
                        + String(currentdate.getMonth()+1).padStart(2, '0')  + "/"
                        + currentdate.getFullYear() + " @ "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
        return datetime;
    }

exports.getUnixTimeStampInSec      = Math.round((new Date()).getTime() / 1000);

exports.getUnixTimeStampInMilliSec = Math.round((new Date()).getTime());

exports.getDaysInMonth = function (m, y) {
    return m===2 ? y & 3 || !(y%25) && y & 15 ? 28 : 29 : 30 + (m+(m>>3)&1);
}

exports.getCurrentYear = function(){
    var currentdate = new Date();
    return currentdate.getFullYear();
}

exports.getCurrentMonth = function(){
    var currentdate = new Date();
    return currentdate.getMonth()+1;
}
month = ["Dummy","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

exports.getFromRawDate = function(rawDate , operation){
    var rawDateSplited = rawDate.split(" ");
    var dateSplited = rawDateSplited[0].split("/");
    if(operation === "getDateNumber") return dateSplited[0];
    if(operation === "getMonthName"){
        var monthIndex = dateSplited[1];
        return month[monthIndex];
    }
    if(operation === "getMonthNumber") return dateSplited[1];
    if(operation === "getYearNumber") return dateSplited[2];
    else return rawDateSplited[2]
}

exports.getFromTime = function(time , operation){
    if(operation === "hour") return time.split(":")[0]
    if(operation === "minutes") return time.split(":")[1]
    if(operation === "seconds") return time.split(":")[2]
}

exports.getDate = function(rawDate){
    var rawDateSplited = rawDate.split(" ");
    return rawDateSplited[0]
}

exports.getDateRevWithHipen = function(rawDate){
    var rawDateSplited = rawDate.split(" ");
    var dateSplited = rawDateSplited[0].split("/"),
        dd = dateSplited[0],
        mm = dateSplited[1],
        yyyy = dateSplited[2];
    return (yyyy + "-" + mm + "-" + dd);
}


exports.getDay = function(rawDate){
    var localMonth = getFromRawDate(rawDate , "getMonthNamme"),
        localDate = getFromRawDate(rawDate , "getDateNumber"),
        localYear = getFromRawDate(rawDate , "getYearNumber"),
        arg = localMonth + " " + localDate + "," + localYear;

    const d = new Date(arg);
    return d.getDay();

}