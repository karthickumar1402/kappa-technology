const jwt = require("jsonwebtoken")
//const axios = require('axios')

module.exports =  function (req , res , next)
{
   let token = req.headers["authorization"]
   if(!token){
      return res.json({error : "User not Authenticated"});
   }
   else{
      token = token.split(" ")[1];

      jwt.verify(token , process.env.ACCESS_TOKEN_SECRET , (err , user) => {
         if(!err){
            req.user = user
            next();
         }
         else{
            return res.json({error : "Error with Auth Key"});
            // console.log("Access key expired. Calling refreshToken");
            // axios
            //    .post('http://localhost:3000/refreshAccessToken', {
            //       refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoia2FydGhpY18wMDciLCJpYXQiOjE2NDE2NjcxNDYsImV4cCI6MTY0MTY3NDM0Nn0.mQdQmK2Xfeeg0cdiFkZcJ1OurKCNuYmjZfsDA8KIjds'
            //       })
            //    .then(resp => {
            //       console.log("///////////////////////////////////////////////////")
            //       console.log(resp.data.accessToken)
            //       console.log("///////////////////////////////////////////////////");

            //       jwt.verify(resp.data.accessToken , process.env.ACCESS_TOKEN_SECRET , (err , user) => {
            //          if(!err){
            //             req.user = user
            //             next();
            //          }
            //          else{
            //             return res.json({error : "Error with Auth Key1"});
            //          }
            //    })
            //    .catch(error => {
            //       console.error(error)
            //       return res.json({error : "Error with Auth Key2"});
            //    })
            // })
         }
      })
   }
}