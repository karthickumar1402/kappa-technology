const { createLogger, format, transports } = require("winston");

const logLevels = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5,
};

const logger = createLogger({
    levels: logLevels,
    format: format.combine(format.timestamp(), format.json()),
    transports: [new transports.Console()],
    //exceptionHandlers: [new transports.Console()],
    //rejectionHandlers: [new transports.Console()],
    exceptionHandlers: [new transports.File({ filename: "exceptions.log" })],
    rejectionHandlers: [new transports.File({ filename: "rejections.log" })],
});

// const logger = createLogger({
//     levels: logLevels,
//     transports: [new transports.Console()],
//   });
  
  
//   const logger = createLogger({
//     format: format.combine(format.timestamp(), format.json()),
//     transports: [new transports.Console({})],
//   });
  
//   const logger = createLogger({
//       level: "warn",
//       levels: logLevels,
//       transports: [new transports.Console({ level: "info" })],
//     });
  

module.exports = logger
