require("dotenv").config()

const nodemailer = require('nodemailer');
const log = require('../Middlewares/logger')

exports.sendmail = function(toMail , amount , totalList, hotel_name , isUpdate){
  let msg = ""

  if(isUpdate){
    msg = "<h3>Updated Order for </h3><br>"
  }
  msg = msg+"<h2>"+hotel_name+"</h2><br><br>";

  for(var i=0 ; i<totalList.length ; i++){
    msg = "<br> name : "+totalList[i]["item_name"] + "<br> count : "+totalList[i]["count"] + "<br> rate : "+totalList[i]["amount"]+"<br>------------------" + msg;
  }
    msg = msg + "<br><b> Total Amount = Rs." + amount + "</b>"

    const transporter = nodemailer.createTransport({
      host: process.env.NODEMAILER_HOST,
      port: process.env.NODEMAILER_PORT,
      auth: {
        user: process.env.NODEMAILER_MAIL_ID,
        pass: process.env.NODEMAILER_PASSWORD,
      },
    });

    transporter.sendMail({ 
      from: '"KAPPA TECHNOLOGY" ' + process.env.NODEMAILER_MAIL_ID, // sender address
      to: toMail, // list of receivers
      subject: hotel_name + " Bill", // Subject line
      //text: "This is auto generated bill", // plain text body
      html: msg,
    }).then(info => {
      log.info({info});
    }).catch(console.error);

}



