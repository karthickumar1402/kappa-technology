const jwt = require("jsonwebtoken")

exports.generateAccessToken = function (user) {
return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn: "3h"})
}

exports.generateRefreshToken = function (user) {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {expiresIn: "6h"})
    }

exports.checkToken = function(token){
    return jwt.verify(token , process.env.ACCESS_TOKEN_SECRET )
}
// module.exports = generateAccessToken
// module.exports = generateRefreshToken

exports.parseJwt = function (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};