const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/analytics/date" ,auth, async(req , res) => {

        let merchant_id = req.body.merchant_id,
            fromDate = req.body.fromDate, //yyyy-mm-dd
            toDate = req.body.toDate;

        if(merchant_id === "") merchant_id = "noMid";
        if(fromDate === "") fromDate = "2022-01-01";
        if(toDate === "") toDate = "2122-01-01";

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : dateAnalytics " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, mmaResult) => {

                if (err) {
                    log.error("Code : dateAnalytics " + err)
                    return res.sendStatus(500);
                }

                log.info("Code : dateAnalytics , DB fetch : Query : " + sql_query)

                if(mmaResult.length == 0){
                    connection.release();
                    log.error("Code : dateAnalytics , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"})
                }
                else{

                    let sql_command = "SELECT * from " + merchant_id + " where date_created >= ? AND date_created <= ?"
                    let sql_query   = mysql.format(sql_command,[fromDate,toDate])

                    await connection.query (sql_query, async (err, results) => {

                        if (err) {
                            log.error("Code : dateAnalytics " + err);
                            return res.sendStatus(500);
                        }

                        log.info("Code : dateAnalytics , DB fetch : Query : " + sql_query)

                        connection.release();

                        if(results.length == 0){
                            log.info("Code : dateAnalytics , request : " + JSON.stringify(req.body) + " , response : {\\\"result\\\" : 0}");
                            return res.json({result : 0})
                        }
                        else{
                            var totalIncome = 0 ,
                                dineIn = 0,
                                takeAway = 0,
                                items_dictionary_count = {},
                                items_dictionary_amount = {},
                                customer_mailId = {};

                            for (var i = 0; i < results.length; i++) {
                                var order_id        = results[i].order_id,
                                    status          = results[i].status,
                                    order_amount    = results[i].order_amount,
                                    items_ordered   = results[i].items_ordered,
                                    order_mode      = results[i].order_mode,
                                    customer_mail   = results[i].customer_mail;

                                items_ordered = JSON.parse(items_ordered);

                                let menu_details = JSON.parse(mmaResult[0].menu_details);

                                //totalIncome Calculation
                                totalIncome = totalIncome + order_amount;

                                //total IN and OUT calculation
                                if(order_mode == "IN")  dineIn++;
                                else takeAway++;

                                //items dict insert / update for items
                                for(var newItem in items_ordered){
                                    if(items_dictionary_count.hasOwnProperty(newItem)){ //since count dict and amount dict are same

                                        items_dictionary_count[newItem] = items_dictionary_count[newItem] + items_ordered[newItem]; //count

                                        items_dictionary_amount[newItem] = items_dictionary_amount[newItem] + (menu_details[newItem] * items_ordered[newItem]); //amount
                                    }

                                    else{ //new insert
                                        var temp_Json_count = { [newItem]  : items_ordered[newItem] };
                                        items_dictionary_count = Object.assign(items_dictionary_count,temp_Json_count);

                                        var temp_Json_amount = { [newItem]  : (menu_details[newItem] * items_ordered[newItem]) };
                                        items_dictionary_amount = Object.assign(items_dictionary_amount,temp_Json_amount);
                                    }
                                }

                                //Highest Item sold
                                //Highest income made by an item for the given period
                                //Lowest Item sold
                                //Lowest income made by an item for the given period

                                //customer
                                if(customer_mailId.hasOwnProperty(customer_mail)){ //count change
                                    customer_mailId[customer_mail] = customer_mailId[customer_mail] + 1;
                                }

                                else{ //new update
                                    var temp_Json = { [customer_mail]  : 1 }
                                    customer_mailId = Object.assign(customer_mailId,temp_Json)
                                }

                            }
                            let resp = {
                                "total_orders" : results.length,
                                "total_income" : totalIncome,
                                "DineIn_count" : dineIn,
                                "takeAway_count" : takeAway,
                                "items_count_list" : items_dictionary_count,
                                "items_amount_list" : items_dictionary_amount,
                                "customer_count_list" : customer_mailId
                            }

                            log.info("Code : dateAnalytics , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : resp}));
                            return res.json({result : resp});
                        }
                    })
                }
            })
        })
    })
}