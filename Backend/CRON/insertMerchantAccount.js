const mysql = require("mysql")
const log = require('../Middlewares/logger')
const {getFromRawDate , getDateRevWithHipen} = require('../Middlewares/dateTime')

module.exports = function(app , db){
    app.post("/v1/internal/cron/insertMerchantAccount" , async(req , res) => {

        let merchant_id = req.body.merchant_id,
            passphrase = req.body.secretInternalPass;

        if(passphrase === process.env.INTERNALPASSWORD){
            let sql_command = "SELECT * from order_reference where merchant_id = ?"
            let sql_query   = mysql.format(sql_command,[merchant_id])

            log.info("Code : cronAPI , Going to fetch order details for merchant "+ merchant_id);

            db.getConnection ( async (err, connection)=> {
                if (err) {
                    log.fatal("Code : cronAPI " + err);
                    let response = {
                        "canPushNext" : false,
                        "message" : "MySQL Database Unavailable during " + merchant_id
                    }
                    log.fatal("Code : cronAPI , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : response}))
                    return res.json({result : response});
                }

                await connection.query (sql_query, async (err, results) => {

                    if (err) {
                        log.fatal("Code : cronAPI " + err)
                        let response = {
                            "canPushNext" : false,
                            "message" : "Select Query Error for " + merchant_id
                        }
                        log.fatal("Code : cronAPI , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : response}))
                        return res.json({result : response});
                    }

                    log.info("Code : cronAPI , DB fetch : Query : " + sql_query)

                    if(results.length == 0){
                        let response = {
                            "canPushNext" : true,
                            "message" : "No data to push for " + merchant_id
                        }
                        log.info("Code : cronAPI , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : response}))
                        return res.json({result : response});
                    }
                    else{
                    //insert in respective merchant account
                        for (var i = 0; i < results.length; i++) {
                            var order_id = results[i].order_id,
                                status = results[i].status,
                                order_amount = results[i].order_amount,
                                items_ordered = results[i].items_ordered,
                                order_mode = results[i].order_mode,
                                created_at = results[i].created_at,
                                customer_mail = results[i].customer_mail;

                            var time_created = getFromRawDate(created_at , "getTime"),
                                date_created = getDateRevWithHipen(created_at);

                            let sql_command = "INSERT INTO "+ merchant_id + " (order_id, order_amount, items_ordered, order_mode, status, date_created, time_created, customer_mail) VALUE (?,?,?,?,?,?,?,?)";
                            let sql_query   = mysql.format(sql_command,[order_id, order_amount, items_ordered, order_mode, status, date_created, time_created, customer_mail]);

                            await connection.query(sql_query , async(err , insertResult) => {

                                if (err) {
                                    log.fatal("Code : cronAPI " + err)
                                    let response = {
                                        "canPushNext" : false,
                                        "message" : "Insert Query Error for " + merchant_id
                                    }
                                    log.fatal("Code : cronAPI , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : response}))
                                    return res.json({result : response});
                                }

                                log.info("Code : cronAPI , DB insert : Query : " + sql_query);

                                log.info("Code : cronAPI , Cron task : order_id : " + order_id + " added Succesfully into table : " + merchant_id);
                            })
                        }
                        connection.release();
                        let response = {
                            "canPushNext" : true,
                            "message" : "All Datas Inserted to table " + merchant_id
                        }
                        log.info("Code : cronAPI , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : response}))
                        return res.json({result : response});
                    }
                })
            })
        }
        else{
            log.fatal("Code : cronAPI , Intruder alert detected. Passphrase used : " + passphrase + " for merchant " + merchant_id);
            res.sendStatus(500);
        }
    })
}