// require("dotenv").config()
const mysql = require("mysql")
const bcrypt = require("bcrypt")
const {parseJwt} = require("../Middlewares/jwtRelated")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

//CREATE MASTER USER
module.exports = function (app , db){

   app.post("/v1/owner/register", async (req,res) => {

      let   name = req.body.name,
            phn_number = req.body.phn_number,
            merchant_id = req.body.unique_id,
            hashedPassword = await bcrypt.hash(req.body.password,10),
            restaurant_name =req.body.restaurant_name,
            address = req.body.address;

      address = JSON.stringify(address);

      db.getConnection( async (err, connection) => {
         if (err) {
            log.fatal("Code : register " + err)
            return res.sendStatus(500);
        }
         const sqlSearch = "SELECT * FROM merchant_menu_account WHERE phone_number = ?"
         const search_query = mysql.format(sqlSearch,[phn_number])

         await connection.query (search_query, async (err, mmaResult) => {
            if (err) {
               log.error("Code : register " + err)
               return res.sendStatus(500);
           }

            if (mmaResult.length == 0) {
               connection.release()
               log.error("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
               return  res.json({error : "No account found. Please add account first"})
            }
            else {

               if (merchant_id != mmaResult[0].merchant_id){
                  connection.release()
                  log.error("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "Incorrect Secret key"}))
                  return  res.json({error : "Incorrect Secret key"})
               }

               const sqlSearch = "SELECT * FROM user_table WHERE phone_number = ?"
               const search_query = mysql.format(sqlSearch,[phn_number])
               const sqlInsert = "INSERT INTO user_table VALUES (0,?,?,?,?,?,?)"
               const insert_query = mysql.format(sqlInsert,[name, phn_number, hashedPassword, restaurant_name, address, mmaResult[0].merchant_id])

               await connection.query (search_query, async (err, result) => {
                  if (err) {
                     log.error("Code : register " + err)
                     return res.sendStatus(500);
                 }

                  if (result.length != 0) {
                     connection.release()
                     log.error("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "User already exists"}))
                     return res.json({error : "User already exists"});
                  }
                  else {
                     await connection.query (insert_query, (err, result)=> {
                        connection.release()
                        if (err) {
                           log.error("Code : register " + err)
                           return res.sendStatus(500);
                       }
                        log.info("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "User Created for user_id : " + phn_number}))
                        return res.json({result : "User Created for user_id : " + phn_number});
                     })
                  }
               })
            }
         })
      })
   })
}