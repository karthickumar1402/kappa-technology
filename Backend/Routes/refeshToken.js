const jwt = require("jsonwebtoken")
const { generateAccessToken ,generateRefreshToken} = require("../Middlewares/jwtRelated")
const log = require('../Middlewares/logger')

module.exports = function(app){
    app.post("/refreshAccessToken" , (req , res) => {
        let refreshToken = req.body.refreshToken

        if(!refreshToken){
            return res.json({error : "User not Authenticated"});
        }
        else{
            jwt.verify(refreshToken , process.env.REFRESH_TOKEN_SECRET , (err , user) => {
                if(!err){
                    let accessToken = generateAccessToken({user: user.name})
                    return res.json({accessToken : accessToken})
                }
                else{
                    return res.json({error : "User not Authenticated"});
                }
            })
        }
    })
}