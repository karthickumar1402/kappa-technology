const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/markPaid" ,auth, async(req , res) => {
        let merchant_id     = req.body.merchant_id,
            order_id        = req.body.order_id;

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : markPaid " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {

                if (err) {
                    log.error("Code : markPaid " + err)
                    return res.sendStatus(500);
                }
                log.info("Code : markPaid , DB fetch : Query : " + sql_query)
                if(result.length == 0){
                    connection.release();
                    log.error("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"});
                }

                let sql_command = "SELECT * FROM order_reference where merchant_id = ? and order_id = ?"
                    sql_query   = mysql.format(sql_command,[merchant_id, order_id])

                await connection.query (sql_query, async (err, searchResult) => {

                    if (err) {
                        log.error("Code : markPaid " + err)
                        return res.sendStatus(500);
                    }

                    log.info("Code : markPaid , DB fetch : Query : " + sql_query)

                    if(searchResult == 0){
                        connection.release();
                        log.error("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No order found"}))
                        return res.json({error : "No order found"});
                    }


                    let sql_command = "UPDATE order_reference SET status = ? where merchant_id = ? and order_id = ?"
                        sql_query   = mysql.format(sql_command,["PAID", merchant_id, order_id])


                    await connection.query (sql_query, async (err, result) => {
                        connection.release();

                        if (err) {
                            log.error("Code : markPaid " + err)
                            return res.sendStatus(500);
                        }

                        log.info("Code : markPaid , DB update : Query : " + sql_query)

                        let response = {"order_id" : order_id , amount : searchResult[0].order_amount, order_status : "PAID"};

                        log.info("Code : register , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({"result" : response}))
                        return res.json({"result" : response});
                    })
                })
            })
        })
    })
}