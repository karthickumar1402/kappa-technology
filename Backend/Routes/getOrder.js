const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/getOrders" ,auth, async(req , res) => {
        let merchant_id = req.body.merchant_id,
            order_status   = req.body.order_status;

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : getOrder " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {

                if (err) {
                    log.error("Code : getOrder " + err)
                    return res.sendStatus(500);
                }

                log.info("Code : getOrder , DB fetch : Query : " + sql_query);
                if(result.length == 0){
                    connection.release();
                    log.error("Code : getOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"})
                }

                let sql_command = "SELECT * from order_reference where merchant_id = ? and status = ?"
                    sql_query   = mysql.format(sql_command,[merchant_id , order_status])


                await connection.query (sql_query, async (err, result) => {
                    connection.release();

                    log.info("Code : getOrder , DB fetch : Query : " + sql_query)

                    if (err) {
                        log.error("Code : getOrder " + err)
                        return res.sendStatus(500);
                    }

                    let result_array = [];

                    for(var orders = 0 ; orders < result.length ; orders++){
                        let responseJson = {
                            "order_id"      : result[orders].order_id,
                            "amount"        : result[orders].order_amount,
                            "items"         : result[orders].items_ordered,
                            "created_at"    : result[orders].created_at
                        }
                        result_array.push(responseJson);
                    }

                    let response = {"count" : result_array.length ,"result" : result_array};
                    log.error("Code : getOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify(response))
                    return res.json(response)
                })
            })
        })

    })
}