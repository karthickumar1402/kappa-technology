const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/addMenu" ,auth, async(req , res) => {
        let merchant_id = req.body.merchant_id;
        let menu = JSON.stringify(req.body.menu_details)

        if(merchant_id === "" || merchant_id === null || merchant_id === undefined) {
            log.error("Code : addMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "merchant_id is missing"}));
            return res.json({error : "merchant_id is missing"});
        }

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : addMenu " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {

                if (err) {
                    log.error("Code : addMenu " + err)
                    return res.sendStatus(500);
                }

                log.info("Code : addMenu , DB fetch : Query : " + sql_query)

                if(result.length == 0){
                    connection.release();
                    log.error("Code : addMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return res.json({error : "No account found. Please add account first"});
                }
                else{

                    sql_command = "UPDATE merchant_menu_account SET menu_details = ? where merchant_id = ?"
                    sql_query   = mysql.format(sql_command,[menu ,merchant_id])

                    await connection.query(sql_query , async(err , result) => {
                        connection.release();

                        if (err) {
                            log.error("Code : addMenu " + err)
                            return res.sendStatus(500);
                        }

                        log.info("Code : addMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "Menu added successfully"}))
                        return res.json({result : "Menu added successfully"})
                    })
                }
            })
        })
    })
}