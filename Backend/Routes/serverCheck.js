const log = require('../Middlewares/logger')

module.exports = function(app){
    app.get("/v1/serverCheck", (req , res) => {
        log.info("Server Check Called");
        res.send('SERVER UP');
    });
}