var _ = require('underscore');
const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/updateMenu" ,auth, async(req , res) => {
        let merchant_id = req.body.merchant_id;
        let newMenu = req.body.menu_details

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : updateMenu " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {

                if (err) {
                    log.error("Code : updateMenu " + err)
                    return res.sendStatus(500);
                }
                log.info("Code : updateMenu , DB fetch : Query : " + sql_query)

                if(result.length == 0){
                    connection.release();
                    log.error("Code : updateMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"});
                }
                else{
                    let oldMenu = JSON.parse(result[0].menu_details);
                    let oldMenuTemp = result[0].menu_details

                    for(var key in newMenu){
                        if(oldMenu.hasOwnProperty(key) && oldMenu[key] === newMenu[key]) continue //duplicate

                        else if(oldMenu.hasOwnProperty(key) && oldMenu[key] != newMenu[key]){     //amount change
                            oldMenu[key] = newMenu[key]
                        }
                        else{ //new update
                            var temp_Json = { [key]  : newMenu[key] }
                            oldMenu = Object.assign(oldMenu,temp_Json)
                        }
                    }
                    //finalMenu = Object.assign(oldMenu,newMenu)

                    finalMenu = JSON.stringify(oldMenu)

                    if (_.isEqual(oldMenuTemp,finalMenu)) {
                        log.info("Code : getMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({Error : "Cannot Insert Duplicate"}))
                        return res.json({Error : "Cannot Insert Duplicate"})
                    }


                    sql_command = "UPDATE merchant_menu_account SET menu_details = ? where merchant_id = ?"
                    sql_query   = mysql.format(sql_command,[finalMenu ,merchant_id])

                    await connection.query(sql_query , async(err , result) => {
                        connection.release();

                        if (err) {
                            log.error("Code : updateMenu " + err)
                            return res.sendStatus(500);
                        }
                        log.info("Code : updateMenu , DB update : Query : ",sql_query);
                        log.info("Code : getMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "Menu updated successfully"}))
                        return res.json({result : "Menu updated successfully"})
                    })
                }
            })
        })
    })
}