const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/getMenu" ,auth, async(req , res) => {

        let merchant_id = req.body.merchant_id;
        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : getMenu " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {
                connection.release();

                if (err) {
                    log.error("Code : getMenu " + err)
                    return res.sendStatus(500);
                }

                log.info("Code : getMenu , DB fetch : Query : " + sql_query)

                if(result.length == 0){
                    log.error("Code : getMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return res.json({error : "No account found. Please add account first"})
                }

                log.info("Code : getMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : result[0].menu_details}))
                return res.json({result : JSON.parse(result[0].menu_details)});
            })
        })
    })
} 