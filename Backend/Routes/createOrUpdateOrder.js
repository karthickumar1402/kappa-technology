const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const {getCurrentDateTime} = require("../Middlewares/dateTime")
const {sendmail} = require('../Middlewares/notification')
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/orders" ,auth, async(req , res) => {
        let merchant_id = req.body.merchant_id,
            order_id = req.body.order_id,
            items_ordered = req.body.items_ordered,
            order_mode = req.body.order_mode,
            customer_mailId = req.body.customer_mailId;

            let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?",
                sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : createOrUpdateOrder " + err)
                return res.sendStatus(500);
            }

            await connection.query(sql_query , async(err , mmaResult) => {

                if (err) {
                    log.error("Code : createOrUpdateOrder " + err)
                    return res.sendStatus(500);
                }

                log.info("Code : createOrUpdateOrder , DB fetch : Query : " + sql_query)

                if(mmaResult == 0) {
                    connection.release();
                    log.error("Code : createOrUpdateOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"})}

                let menu_template = JSON.parse(mmaResult[0].menu_details);

                let sql_command = "SELECT * from order_reference where merchant_id = ? and order_id = ?"
                    sql_query   = mysql.format(sql_command,[merchant_id , order_id])


                await connection.query (sql_query, async (err, result) => {

                    if (err) {
                        log.error("Code : createOrUpdateOrder " + err)
                        return res.sendStatus(500);
                    }

                    log.info("Code : createOrUpdateOrder , DB fetch : Query : " + sql_query)

                    if(result.length == 0){
                        //New Order

                        let created_at = getCurrentDateTime();
                        var order_amount = 0;

                        for(var items in items_ordered){
                            order_amount = order_amount + (menu_template[items] * items_ordered[items])
                        }

                        items_ordered= JSON.stringify(items_ordered);

                        let sql_command = "INSERT INTO order_reference (merchant_id, order_id, order_amount, items_ordered, order_mode, status, created_at, customer_mail) VALUE (?,?,?,?,?,?,?,?)"
                            sql_query   = mysql.format(sql_command,[merchant_id, order_id, order_amount, items_ordered, order_mode, "PENDING", created_at,customer_mailId])

                        await connection.query(sql_query , async(err , result) => {
                            connection.release();

                            log.info("Code : createOrUpdateOrder , DB insert : Query : ",sql_query);

                            if (err) {
                                log.error("Code : createOrUpdateOrder " + err)
                                return res.sendStatus(500);
                            }

                            let mailRes = [];
                            items_ordered = JSON.parse(items_ordered);
                            for(var item in items_ordered){
                                let resJson = {
                                    "item_name" : item,
                                    "count" : items_ordered[item],
                                    "amount" : menu_template[item] * items_ordered[item]
                                }
                                mailRes.push(resJson);
                            }

                            sendmail(customer_mailId, order_amount, mailRes, mmaResult[0].hotel_name , false);
                            log.info("Code : createOrUpdateOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "Order Added successfully" , order_id : order_id , order_status : "PENDING" , amount : order_amount}))
                            return res.json({result : "Order Added successfully" , order_id : order_id , order_status : "PENDING" , amount : order_amount})
                        })
                    }
                    else{
                        //update Order

                        let oldAmount = result[0].order_amount,
                            order_status = result[0].status;

                        if( order_status === "PENDING"){ //Pending check

                            for(var items in items_ordered){
                                oldAmount = oldAmount + ( menu_template[items] * items_ordered[items] ) //amount = amount + (template_amount * count)
                            }

                            /////////////////////////////////////

                            let oldItemsOrdered = JSON.parse(result[0].items_ordered);

                            for(var newItem in items_ordered){
                                if(oldItemsOrdered.hasOwnProperty(newItem)){ //count change
                                    oldItemsOrdered[newItem] = oldItemsOrdered[newItem] + items_ordered[newItem]
                                }

                                else{ //new update
                                    var temp_Json = { [newItem]  : items_ordered[newItem] }
                                    oldItemsOrdered = Object.assign(oldItemsOrdered,temp_Json)
                                }
                            }

                            finalitems = JSON.stringify(oldItemsOrdered)

                            sql_command = "UPDATE order_reference SET order_amount = ? , items_ordered = ? where order_id = ? and merchant_id = ?"
                            sql_query   = mysql.format(sql_command,[oldAmount, finalitems, order_id, merchant_id])

                            await connection.query(sql_query , async(err , result) => {
                                connection.release();

                                if (err) {
                                    log.error("Code : createOrUpdateOrder " + err)
                                    return res.sendStatus(500);
                                }

                                log.info("Code : createOrUpdateOrder , DB update : Query : ",sql_query);

                                let mailResElse = [];

                                for(var item in oldItemsOrdered){
                                    let resJson = {
                                        "item_name" : item,
                                        "count" : oldItemsOrdered[item],
                                        "amount" : menu_template[item] * oldItemsOrdered[item]
                                    }
                                mailResElse.push(resJson);
                                }
                                sendmail(customer_mailId, oldAmount, mailResElse, mmaResult[0].hotel_name , true);
                                log.info("Code : createOrUpdateOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "Order Added successfully" , order_id : order_id , order_status : order_status , amount : oldAmount}))
                                return res.json({result : "Order Added successfully" , order_id : order_id , order_status : order_status , amount : oldAmount})
                            })
                        }
                        else{
                            log.error("Code : createOrUpdateOrder , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "Order Closed" , order_id : order_id , order_status : order_status}))
                            return res.json({error : "Order Closed" , order_id : order_id , order_status : order_status})
                        }
                    }
                })
            })
        })
    })
}