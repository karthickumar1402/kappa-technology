const mysql = require("mysql")
const auth = require("../Middlewares/VerifyMerchantAuth")
const log = require('../Middlewares/logger')

module.exports = function(app , db){
    app.post("/v1/deleteMenu" ,auth, async(req , res) => {
        let merchant_id = req.body.merchant_id;
        let unWantedMenu = req.body.menu_details

        let sql_command = "SELECT * from merchant_menu_account where merchant_id = ?"
        let sql_query   = mysql.format(sql_command,[merchant_id])

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : deleteMenu " + err)
                return res.sendStatus(500);
            }

            await connection.query (sql_query, async (err, result) => {

                if (err) {
                    log.error("Code : deleteMenu " + err)
                    return res.sendStatus(500);
                }
                log.info("Code : deleteMenu , DB fetch : Query : " + sql_query)

                if(result.length == 0){
                    connection.release();
                    log.error("Code : deleteMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({error : "No account found. Please add account first"}))
                    return  res.json({error : "No account found. Please add account first"})
                }
                else{
                    let oldMenu = JSON.parse(result[0].menu_details);

                    for (var key in unWantedMenu) {
                        if (oldMenu.hasOwnProperty(key)) {
                                delete oldMenu[key];
                        }
                    }
                    finalMenu = JSON.stringify(oldMenu)

                    sql_command = "UPDATE merchant_menu_account SET menu_details = ? where merchant_id = ?"
                    sql_query   = mysql.format(sql_command,[finalMenu ,merchant_id])

                    await connection.query(sql_query , async(err , result) => {
                        connection.release();

                        if (err) {
                            log.error("Code : deleteMenu " + err)
                            return res.sendStatus(500);
                        }
                        log.info("Code : deleteMenu , DB update : Query : " + sql_query)

                        log.info("Code : deleteMenu , request : " + JSON.stringify(req.body) + " , response : " + JSON.stringify({result : "Menu deleted successfully"}))
                        return res.json({result : "Menu deleted successfully"})
                    })
                }
            })
        })
    })
}