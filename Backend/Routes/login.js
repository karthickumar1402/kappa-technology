const { generateAccessToken ,generateRefreshToken} = require("../Middlewares/jwtRelated")
const mysql = require("mysql")
const bcrypt = require("bcrypt")
const log = require('../Middlewares/logger')

module.exports = function (app , db){
    app.post("/v1/accounts/login", (req, res) => {

        const phn_number    = req.body.phn_number,
              password      = req.body.password;

        log.info("Code : login , logging in phone number : "+ phn_number);

        db.getConnection ( async (err, connection)=> {
            if (err) {
                log.fatal("Code : login " + err)
                return res.sendStatus(500);
            }
            const sqlSearch = "Select * from user_table where phone_number = ?",
                  search_query = mysql.format(sqlSearch,[phn_number])

            await connection.query (search_query, async (err, result) => {
                connection.release()

                if (err) {
                    log.error("Code : login " + err)
                    return res.sendStatus(500);
                }
                if (result.length == 0) {
                    log.error("Code : login , No Record Found in user_table" + phn_number);
                    return res.json({ error : "Username / Password incorrect!"})
                    }
                else {
                    const hashedPassword = result[0].password

                    //get the hashedPassword from result
                    if (await bcrypt.compare(password, hashedPassword)) {
                    const accessToken = generateAccessToken({user: phn_number})
                    const refreshToken = generateRefreshToken({user: phn_number})


                    log.info("Code : login , Login Success. Access token and Refresh token generated");
                    let response = {accessToken: accessToken , refreshToken: refreshToken , merchant_id : result[0].merchant_id};
                    return res.json({"result" : response});
                    }
                    else {
                    log.error("Code : login , Password incorrect for " + phn_number);
                    return res.json({error : "Username / Password incorrect!"})
                    } //end of Password incorrect
                } //end of User exists
            }) //end of connection.query()
        }) //end of db.connection()
    }) //end of app.post()
}