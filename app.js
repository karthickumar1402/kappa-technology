const express = require("express")
const app = express()

app.use(express.json())

const db = require('./dbServer')
const log = require('./Backend/Middlewares/logger')

require('./Backend/Routes/serverCheck')(app);

require('./Backend/Routes/register')(app , db);
require('./Backend/Routes/login')(app , db);
require('./Backend/Routes/refeshToken')(app)

require('./Backend/Routes/getMenu')(app , db);
require('./Backend/Routes/addMenu')(app , db);
require('./Backend/Routes/updateMenu')(app , db);
require('./Backend/Routes/deleteMenu')(app , db);

require('./Backend/Routes/createOrUpdateOrder')(app,db);
require('./Backend/Routes/getOrder')(app,db);
require('./Backend/Routes/markPaid')(app,db);

//internal Cron
require('./Backend/CRON/insertMerchantAccount')(app,db);

//Analytics
require('./Backend/Analytics/dateAnalytics')(app,db);
require('./Backend/Analytics/lastMonth')(app,db);
require('./Backend/Analytics/liveAnalytics')(app,db);

const port = process.env.PORT

log.info("System start")

app.listen(port,
()=> console.log(`Server Started on port ${port}...`))